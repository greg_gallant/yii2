<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '1OPAMEHydCtXM3BWn59s-ZAfW2e4x6Ck',
        ],
    ],
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config = [

        'bootstrap' => ['debug'],
        'modules' => [
            'debug' => [
                'class' => 'yii\debug\Module',
                'allowedIPs' => ['127.0.0.1', '172.22.22.20']
            ]
        ],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
