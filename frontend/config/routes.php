<?php

use yii\web\UrlRule;

/**
 * Much better Yii2 Routes primer:
 * http://www.larryullman.com/2013/02/18/understanding-routes-in-the-yii-framework/
 **/

return array(

    // My articles routes
    //'articles' => 'articles/list',

    // normal routes for CRUD operations
    '<controller:\w+>/<id:\d+>' => '<controller>/view',
    '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',

);

