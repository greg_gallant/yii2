<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\Articles;
use frontend\models\Author;
use frontend\lib\PrefilterBehavior;
use yii\helpers\ArrayHelper;


/**
 * Class ArticlesController
 * @package backend\controllers
 *
 * A techbytes like CMS
 */
class ArticlesController extends Controller
{

    /* Simple filter that spits error logs before validation and after db insertion */
    public function behaviors()
    {
        return [
            'prefilter' => [
                'class' => PrefilterBehavior::className(),

            ]
        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }


    /**
     * Using the search model to list all the articles in the database
     */
    public function actionList()
    {

        $article_model = new Articles();

        $article_listing = $article_model->articleSearch();

        // by default, display the form
        return $this->render('list', [
            'article_model' => $article_model,
            'dataProvider' => $article_listing
        ]);

    }

    /**
     * actionCreate()
     *
     * This is an example of a simple article form created from scratch for purposes of understanding the dynamic model, rules, behaviors etc.
     * There is also the Gii Crud generator which I will use next, this essentially creates a crud for you.
     */
    public function actionCreate()
    {
        $model = new Articles();

        if ($model->load(Yii::$app->request->post())) {

            if ($model->validate())
            {

                $data = Yii::$app->request->post('Articles');

                $model->author_id = isset($data['author_id']) ? $data['author_id'] : null;
                $model->headline = isset($data['headline']) ? $data['headline'] : null;
                $model->subheadline = isset($data['subheadline']) ? $data['subheadline'] : null;
                $model->content = isset($data['content']) ? $data['content'] : null;

                $model->save();

                // instead of refreshing,  display list
                //return $this->refresh();
                return $this->redirect('list',302);

            } else {

                $errors = $model->getErrors();
                print_r($errors, true);

            }
        }

        /* Select Box Build */
        $author_list = $this->createListBox();

        // by default, diplay the form
        return $this->render('create', [
            'model' => $model,
            'author_list' => $author_list,
        ]);
    }


    /**
     * Create author list to send to selectbox view
     */
    public function createListBox()
    {
        $author = new Author;
        $authors_model = $author->getAuthors();

        $authors = array();
        foreach($authors_model as $eachAuth)
        {
            $keyval = [$eachAuth['id'] => $eachAuth['fullname']];
            array_push($authors, $keyval);
        }

        return $authors;
    }

    /**
     * End notes on Yii's different types of rendering...
     *
     * render(): renders a named view and applies a layout to the rendering result.
     * renderPartial(): renders a named view without any layout.
     * renderAjax(): renders a named view without any layout, and injects all registered JS/CSS scripts and files. It is usually used in response to AJAX Web requests.
     * renderFile(): renders a view specified in terms of a view file path or alias.
     * renderContent(): renders a static string by embedding it into the currently applicable layout. This method is available since version 2.0.1.
     */
     /* Views: http://www.yiiframework.com/doc-2.0/guide-structure-views.html */

}
