<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\Author;


/**
 * Class AuthorController
 * @package backend\controllers
 */
class AuthorController extends Controller
{

    public function behaviors()
    {
        return [];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function actionIndex()
    {
        $model = new Author();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate())
            {
                $model->save();
                return $this->refresh();
            }
        }

        // by default, diplay the form
        return $this->render('author', [
            'model' => $model,
        ]);
    }

}


