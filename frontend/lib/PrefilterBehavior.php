<?php

namespace frontend\lib;

use yii\db\ActiveRecord;
use yii\base\Behavior;

class PrefilterBehavior extends Behavior
{

    /**
     * Writing a sample pre and post filter using Yii's ActiveRecord
     *  behaviors testing
     */
    public function events() {

        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
        ];

    }

    public function beforeValidate ($event)
    {
        error_log('I happen before any validation.');
    }

    public function afterInsert($event)
    {
        error_log('Message inserted, test for something.');

    }

}

