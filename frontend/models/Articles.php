<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * Class Articles
 * Docs on Dynamic Models: http://www.yiiframework.com/doc-2.0/yii-base-dynamicmodel.html#validateData()-detail
 * Docs on Rules Formatting: http://www.yiiframework.com/doc-2.0/yii-base-model.html#rules()-detail
 *
 * @property integer $id
 * @property integer $author_id
 * @property string $headline
 * @property string $subheadline
 * @property string $content
 */
class Articles extends ActiveRecord
{

    protected $author_id;

    protected $headline;

    protected $subheadline;

    protected $content;

    public static function tableName()
    {
        return 'articles';
    }


    public function rules()
    {

        return[
           [['author_id'],   'integer'],
           [['headline'],   'string', 'max' => 25],
           [['subheadline'],'string', 'max' => 100],
           [[ 'content'], 'string', 'max'=> 65000],
           //[[ 'headline', 'content' ], 'required'] -- There's something broken here..
        ];

        //end notes: http://code.tutsplus.com/tutorials/programming-with-yii2-validations--cms-23418
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ArticleID',
            'author_id' => 'AuthorID',
            'headline' => 'Headline',
            'subheadline' => 'Blurb',
            'content' => 'Captivating Content'
        ];

    }

    public function getArticles() {
        return $this->hasMany(Articles::className(), []);
    }


    /* This is a function to be used to return articles with filters.
     * Also using the ActiveDataProvider.
     *
     * Testing the Yii2 active record database
     */
    public function articleSearch($params = null)
    {

        $query = Articles::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        /* Without params, return all (Select *...) */
        if (!($this->load($params)) && $this->validate()) {
            return $dataProvider;
        }

        /**
         * Adding a filter to get specific article
         * Will probably add a categories column for further testing
         */
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        return $dataProvider;

    }
/*

$query->andFilterWhere(['like', 'brand', $this->brand])
    ->andFilterWhere(['like', 'model', $this->model]);

return $dataProvider;
*/

}
