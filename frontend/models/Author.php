<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;
use yii\data\BaseDataProvider;
use yii\data\SqlDataProvider;

/**
 * Class Articles
 * Docs on Dynamic Models: http://www.yiiframework.com/doc-2.0/yii-base-dynamicmodel.html#validateData()-detail
 * Docs on Rules Formatting: http://www.yiiframework.com/doc-2.0/yii-base-model.html#rules()-detail
 *
 * @property integer $id
 * @property string $fullname
 * @property string $status
 */
class Author extends ActiveRecord
{

    protected $id;

    protected $fullname;

    protected $status;

    public static function tableName()
    {
        return 'author';
    }

    public function rules()
    {
        return[
           [['id'],   'integer'],
           [['fullname'],   'string', 'max' => 100],
           [['status'],'integer', 'max' => 2],
        ];
    }

    public function attributeLabels()
    {
        return [
            'fullname' => 'Full Name',
            'status' => 'Status'
        ];
    }

    /**
     *
     * -== 3 Types of Dataproviders ==-
     *  ActiveDataProvider -- creates ActiveRecord Object models
     *  ArrayDataProvider -- Creates Array models (also customizable - sort, pagination etc.)
     *  SqlDataProvider -- custom SQL statements (also customizable - sort, pagination etc.)
     *  Can create Custom data providers by extending BaseDataProvider
     *   -> Docs: http://www.yiiframework.com/doc-2.0/guide-output-data-providers.html
     *
     *    @return SqlDataProvider
     */
    public function getAuthors()
    {
        /* Can customize data provider */
        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT id, fullname FROM author WHERE status=:status',
            'params' => [':status' => 1],
            'sort' => [
                'attributes' => [
                    'fullname' => [
                        'asc' => ['fullname' => SORT_ASC],
                        'default' => SORT_ASC
                    ]
                ]
            ]
        ]);

        $author_models = $dataProvider->getModels();

        return $author_models;
    }

    /**
     * Return the name of author via id
     */
    public function getAuthorName($id)
    {

        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT fullname FROM author WHERE id=:id',
            'params' => [':id' => $id]
        ]);

        $author_data = $dataProvider->getModels();

        $author_name = $author_data[0]['fullname'];

        return $author_name;

    }

}
