<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\Articles */
$this->title = 'Articles on Yii 2.0';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <strong>Write an article about Laravel using a Yii form: </strong>

    <div class="articles">

        <?php $form = ActiveForm::begin(['id'=>'articles']); ?>

            <?= $form->field($model, 'author_id')->dropDownList(
                    $author_list,['prompt'=>'Choose Your Author']
                );
            ?>
            <?= $form->field($model, 'headline') ?>
            <?= $form->field($model, 'subheadline') ?>
            <?= $form->field($model, 'content')->textarea(array('rows'=>7, 'cols'=>5)) ?>

            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
            </div>

        <?php //error_log(print_r($form)); ?>

        <?php ActiveForm::end(); ?>

    </div><!-- articles -->

    <code><?= __FILE__ ?></code>
</div>

<!-- Active form notes: http://www.bsourcecode.com/yiiframework2/yii2-0-activeform-input-fields/ -->
