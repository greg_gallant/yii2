<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use frontend\models\Author;


/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\Articles */
$this->title = 'Articles on Yii 2.0';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?> - [<a href="create">Create New Article</a>]</h1>

    <div class="articles">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $article_model,
            'summary'=>'<h2>A List of Articles regarding Yii2 vs. Laravel</h2>',
            'showFooter'=>true,
            'showHeader' => true,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label' => 'Author',
                    'value' =>
                        function($data) {
                            $author = new Author;
                            return $author->getAuthorName($data->author_id);
                        }
                ],
                'headline',
                'subheadline',
                'content',
                ['class' => 'yii\grid\ActionColumn'],
            ],
          ]);
        ?>

    </div><!-- articles -->

    <code><?= __FILE__ ?></code>
</div>
